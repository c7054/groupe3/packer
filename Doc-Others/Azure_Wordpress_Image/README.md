Conditions du déploiement : 

 - VM Kaisen Linux avec Packer + Azure CLI.
 - Créer un répertoire à la racine de Packer, exemple "Packer_Test".
 - Déposer les fichiers dans le nouveau répertoire.

Pré-requis :
 - Récupérer les identifiants azure
  - Dans Azure CLI
   - az login
   - Rentrer ses identifiants
   - Récupérer Tonent ID + Subscription ID (variable id)
  - Modifier le fichier pck.json.pck.hcl
 - Executer les commandes (avec sudo) 
  - Packer init ...
  - Packer fmt ...
  - Packer validate ...

Déploiement : 
 - Packer build ... 

Résultat : 
 - Image créé sur Azure
 - Créer une machine virtuelle avec l'image créé précédemment
 - Accéder à l'ip publique de la vm via un navigateur
 - Tadaaaa.. La page d'installation de Wordpress s'affiche
