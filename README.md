### PACKER

### INSTALLATION DE PACKER SUR DEBIAN

Installer le paquet gnupg, qui sera dépendant pour ajouté la clé pour installer packer
```
apt install gnupg
```

Ici on ajoute la clé du site hashicorp
```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
```

On ajoute ensuite le repo sur votre machine
```
apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com/ $(lsb_release -cs) main"
```

Il faut faire un update sur vos repos
```
apt update
```

Puis installer packer
```
apt install packer
```
