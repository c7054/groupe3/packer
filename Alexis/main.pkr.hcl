source "azure-arm" "debian" {
  subscription_id = "aff804a1-0b5a-4ce6-8c2a-b826dcbe29e9"
  tenant_id = "190ce420-b157-44ae-bc2f-69563baa5a3b"
  use_azure_cli_auth = true
  managed_image_name = "debian"
  managed_image_resource_group_name = "packerressource"

  os_type = "Linux"
  image_publisher = "Debian"
  image_offer = "debian-11"
  image_sku = "11-gen2"

  location = "eastus"
  vm_size = "Standard_B1s"
}

build {
  name = "learn-packer"
  sources = [
    "source.azure-arm.debian"
  ]

provisioner "shell" {
  inline = [
    "sleep 60",
    "sudo apt -y update",
    "sudo apt -y upgrade",
    "sudo apt install -y apache2",
    "sudo apt install -y perl",
  ]
}

provisioner "ansible" {
  playbook_file = "./mnt/ansible/playbook/services/account.yaml"
}
}
