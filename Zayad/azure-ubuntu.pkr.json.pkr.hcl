source "azure-arm" "debian" {
  subscription_id                   = "36a3208f-9672-41cc-b490-a793fc92210d"
  tenant_id                         = "190ce420-b157-44ae-bc2f-69563baa5a3b"
  managed_image_resource_group_name = "rscterra"
  managed_image_name                = "Img_Debian11_template"
  os_type                           = "Linux"
  image_publisher                   = "Debian"
  image_offer                       = "debian-11"
  image_sku                         = "11-gen2"
  use_azure_cli_auth                = true
  location                          = "eastus"
  vm_size                           = "Standard_B1ms"
}
build {
  name    = "learn-packer"
  sources = ["source.azure-arm.debian"]
  provisioner "shell" {
    script = "./deploy_base.sh"
  }
  provisioner "ansible" {
    playbook_file = "/mnt/d/Documents/Documents/School/MSI/DevOps/Outils_DevOps/groupe3/ansible/playbook/services/packer.yml"
  }
}
