#!/bin/bash

#Mise en place du nonactive
sync
export DEBIAN_FRONTEND=noninteractive

#Mise à jours
sync
sudo apt -y update
sync
sudo apt -y upgrade
sync

#Installation pré-requis déploiement ansible
sudo apt -y install python3 ansible
sync
