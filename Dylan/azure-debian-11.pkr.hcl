source "azure-arm" "debian" {
  tenant_id                         = "190ce420-b157-44ae-bc2f-69563baa5a3b"
  subscription_id                   = "b3434983-fbf6-4e47-b164-d209ac4d7d16"
  managed_image_resource_group_name = "rdlpacker"
  managed_image_name                = "Debian"
  os_type                           = "Linux"
  image_publisher                   = "Debian"
  image_offer                       = "debian-11"
  image_sku                         = "11-gen2"
  use_azure_cli_auth                = true
  location                          = "eastus"
  vm_size                           = "Standard_B1ms"
}
build {
  name = "learn-packer"
  sources = ["source.azure-arm.debian"]

provisioner "shell" {
  inline = [ "sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::='--force-confdef' -o Dpkg::Options::='--force-confold' dist-upgrade" ]
  }
provisioner "ansible" {
  playbook_file = "/home/darkzone/git/cesi/cloud/ansible/playbook/services/packer.yml"
}
}
